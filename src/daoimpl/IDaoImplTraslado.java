/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daoimpl;

import dao.IDaoTraslado;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pojo.Traslado;
import util.RandomFileBinarySearch;

/**
 *
 * @author Yasser
 */
public class IDaoImplTraslado implements IDaoTraslado {

    private RandomAccessFile hraf;
    private RandomAccessFile draf;
    private static int SIZE = 259;
    private final String HEADER_FILENAME = "htraslado.dat";
    private final String DATA_FILENAME = "dtraslado.dat";

    public IDaoImplTraslado() {
        RandomFileBinarySearch.SIZE = 4;
    }

    private void open() throws IOException {

        File hfile = new File(HEADER_FILENAME);
        File dfile = new File(DATA_FILENAME);

        if (!hfile.exists()) {
            hfile.createNewFile();
            hraf = new RandomAccessFile(hfile, "rw");
            draf = new RandomAccessFile(dfile, "rw");
            hraf.seek(0);
            hraf.writeInt(0);
            hraf.writeInt(0);
        } else {
            hraf = new RandomAccessFile(hfile, "rw");
            draf = new RandomAccessFile(dfile, "rw");
        }

    }

    private void close() throws IOException {
        if (hraf != null) {
            hraf.close();
            hraf = null;
        }

        if (draf != null) {
            draf.close();
            draf = null;
        }
    }

    @Override
    public Traslado buscarPorId(int id) throws IOException {
        open();
        hraf.seek(0);
        int n = hraf.readInt();
        int k = hraf.readInt();
        if (n == 0) {
            close();
            return null;
        }

        RandomFileBinarySearch.hraf = hraf;
        int pos = RandomFileBinarySearch.runBinarySearchRecursively(id, 0, n);
        if (pos < 0) {
            close();
            return null;
        }
        long hpos = 8 + 4 * (pos);
        hraf.seek(hpos);
        int index = hraf.readInt();

        long dpos = (index - 1) * SIZE;
        draf.seek(dpos);

        Traslado t = new Traslado();
        t.setId(draf.readInt());
        t.setTransaccion(draf.readUTF());
        t.setFechaTransaccion(draf.readLong());
        t.setReferencia(draf.readUTF());
        t.setAlmacenOrigen(draf.readUTF());
        t.setUbicacionOrigen(draf.readUTF());
        t.setAlmacenDestino(draf.readUTF());
        t.setUbicacionDestino(draf.readUTF());

        close();

        return t;
    }

    @Override
    public void save(Traslado t) throws IOException {
        open();
        hraf.seek(0);
        int n = hraf.readInt();
        int k = hraf.readInt();

        long pos = k * SIZE;

        draf.seek(pos);

        draf.writeInt(++k);
        draf.writeUTF(t.getTransaccion());
        draf.writeLong(t.getFechaTransaccion());
        draf.writeUTF(t.getReferencia());
        draf.writeUTF(t.getAlmacenOrigen());
        draf.writeUTF(t.getUbicacionOrigen());
        draf.writeUTF(t.getAlmacenDestino());
        draf.writeUTF(t.getUbicacionDestino());

        hraf.seek(0);
        hraf.writeInt(++n);
        hraf.writeInt(k);

        long hpos = 8 + 4 * (n - 1);
        hraf.seek(hpos);
        hraf.writeInt(k);
        close();
    }

    @Override
    public int update(Traslado t) throws IOException {
        open();
        hraf.seek(0);
        RandomFileBinarySearch.hraf = hraf;
        int n = hraf.readInt();
        int pos = RandomFileBinarySearch.runBinarySearchRecursively(t.getId(), 0, n);
        if (pos < 0) {
            close();
            return pos;
        }
        long hpos = 8 + 4 * (pos);
        hraf.seek(hpos);
        int id = hraf.readInt();

        long dpos = (id - 1) * SIZE;
        draf.seek(dpos);

        draf.writeInt(t.getId());
        draf.writeUTF(t.getTransaccion());
        draf.writeLong(t.getFechaTransaccion());
        draf.writeUTF(t.getReferencia());
        draf.writeUTF(t.getAlmacenOrigen());
        draf.writeUTF(t.getUbicacionOrigen());
        draf.writeUTF(t.getAlmacenDestino());
        draf.writeUTF(t.getUbicacionDestino());

        close();
        return t.getId();
    }

    @Override
    public boolean delete(Traslado t) throws IOException {
        File tmp = new File("tmp.dat");
        try (RandomAccessFile tmpraf = new RandomAccessFile(tmp, "rw")) {
            open();
            hraf.seek(0);
            int n = hraf.readInt();
            int k = hraf.readInt();

            tmpraf.seek(0);
            tmpraf.writeInt(n - 1);
            tmpraf.writeInt(k);
            int j = 0;
            for (int i = 0; i < n; i++) {
                long hpos = 8 + 4 * i;
                hraf.seek(hpos);
                int id = hraf.readInt();
                if (id == t.getId()) {
                    continue;
                }
                long tmpos = 8 + 4 * j++;
                tmpraf.seek(tmpos);
                tmpraf.writeInt(id);
            }
            close();
        }
        close();
        File f = new File(HEADER_FILENAME);
        boolean flag = f.delete();
        if (flag) {
            tmp.renameTo(f);
        } else {
            Logger.getLogger(IDaoImplTraslado.class.getName()).log(Level.SEVERE, "ERROR, no se pudo eliminar el archivo!");
        }
        return flag;
    }

    @Override
    public List<Traslado> findAll() throws IOException {
        open();
        List<Traslado> traslados = new ArrayList<>();
        hraf.seek(0);
        int n = hraf.readInt();

        for (int i = 0; i < n; i++) {
            long hpos = 8 + 4 * i;
            hraf.seek(hpos);
            int id = hraf.readInt();

            long dpos = (id - 1) * SIZE;
            draf.seek(dpos);

            Traslado t = new Traslado();
            t.setId(draf.readInt());
            t.setTransaccion(draf.readUTF());
            t.setFechaTransaccion(draf.readLong());
            t.setReferencia(draf.readUTF());
            t.setAlmacenOrigen(draf.readUTF());
            t.setUbicacionOrigen(draf.readUTF());
            t.setAlmacenDestino(draf.readUTF());
            t.setUbicacionDestino(draf.readUTF());

            traslados.add(t);
        }

        close();
        return traslados;
    }

    public Traslado encontrarUno(int codigo) throws IOException {
        
        List<Traslado> lista = findAll();
        
        for(Traslado e: lista){
            if(e.getId() == codigo){
                return e;
            }
        }
        
        return null;
    }
}
