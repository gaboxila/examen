/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import daoimpl.IDaoImplTraslado;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import pojo.Traslado;

/**
 *
 * @author gabox
 */
public class ControladorFrmTraslado {
    
    IDaoImplTraslado controla;
    
    private String[] header = {
        "ID", "TRANSACCION", "FECHA", "REFERENCIA", "ALMACEN ORIGEN", "UBICACION ORIGEN", 
        "ALMACEN DESTINO", "UBICACION DESTINO"
    };
    
    public ControladorFrmTraslado() {
        controla = new IDaoImplTraslado();
    }
    
    public TableModel getTableModel() throws IOException{
        List<Traslado> lista = controla.findAll();
        Object[][] data = new Object[lista.size()][header.length];
        int i = 0;
        for(Traslado e: lista){
            data[i] = getDatos(e);
            i++;
        }
        return new DefaultTableModel(data, header);
    }
    
    public Object[] getDatos(Traslado t){
        Object[] single = new Object[header.length];
        single[0] = t.getId();
        single[1] = t.getFechaTransaccion();
        single[2] = t.getReferencia();
        single[3] = t.getAlmacenOrigen();
        single[4] = t.getUbicacionOrigen();
        single[5] = t.getAlmacenDestino();
        single[6] = t.getUbicacionDestino();
        return single;
    }
    
    public TableRowSorter getRowSorter()throws IOException{
        return new TableRowSorter(getTableModel());
    }
    
    public Traslado encontrarCodigo(int codigo) throws IOException{
        return controla.encontrarUno(codigo);
    }
}
