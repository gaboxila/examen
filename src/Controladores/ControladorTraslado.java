/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import daoimpl.IDaoImplTraslado;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import pojo.ALMACENDESTINO;
import pojo.ALMACENORIGEN;
import pojo.TRANSACCION;
import pojo.Traslado;
import pojo.UBICACIONDESTINO;
import pojo.UBICACIONORIGEN;

/**
 *
 * @author gabox
 */
public class ControladorTraslado {
    
    IDaoImplTraslado implementa;

    public ControladorTraslado() {
        implementa = new IDaoImplTraslado();
    }
    
    public void guardar(Traslado t) throws IOException{
        implementa.save(t);
    }
    
    public void editar(Traslado t) throws IOException{
        implementa.update(t);
    }
    
    public void guardarEditar(Traslado t) throws IOException{
        Traslado objeto = implementa.buscarPorId(t.getId());
        if(objeto != null){
            editar(t);
        }else{
            guardar(t);
        }
        
    }
    
    public ComboBoxModel getTransaccion() {
        return new DefaultComboBoxModel(TRANSACCION.values());
    }
    
    public ComboBoxModel getAlmacenOrigen(){
        return new DefaultComboBoxModel(ALMACENORIGEN.values());
    }
    
    public ComboBoxModel getUbicacionOrigen(){
        return new DefaultComboBoxModel(UBICACIONORIGEN.values());
    }
    
    public ComboBoxModel getAlmacenDestino(){
        return new DefaultComboBoxModel(ALMACENDESTINO.values());
    }
    
    public ComboBoxModel getUbicacionDestino(){
        return new DefaultComboBoxModel(UBICACIONDESTINO.values());
    }
}
